import { User } from "./types.js";
import fs from "fs/promises";

const DB_FILE_PATH = "./db.json";

export async function getDB() {
  try {
    const data = await fs.readFile(DB_FILE_PATH, "utf8");
    const users: User[] = JSON.parse(data);
    return users;
} catch (err) {
    updateDB([]);
    return [];
  }
}

export function updateDB(users: User[]) {
  const jsonUsers = JSON.stringify(users, null, 2);
  fs.writeFile(DB_FILE_PATH, jsonUsers, { encoding: "utf8", flag: "w" });
}
