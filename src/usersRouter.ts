import express from "express";
import { responseMessage , User } from "./types.js";
import {
  updateUser,
  createUser,
  deleteUser,
  readAllUsers,
  readUser,
} from "./controller.js";


const router = express.Router();

//create user
router.post("/", async (req, res, next) => {
  try {
    const { firstName, lastName, email, password } = req.body;
    const users = await createUser(firstName, lastName, email, password);
    const resCreate: responseMessage = {status:200,message:"create new user",data:users}
    res.send(resCreate);
  } catch (err) {
    next(err);
  }
});

//update
router.put("/:id", async (req, res, next) => {
  try {
    const user = await updateUser(req.params.id, req.body);
    const resCreate: responseMessage = {status:200,message:"user updated",data:user}
    res.send(resCreate);
  } catch (err) {
    next(err);
  }
});

//delete user
router.delete("/:id", async (req, res, next) => {
  try {
    const users = await deleteUser(req.params.id);
    const resCreate: responseMessage = {status:200,message:"user removed",data:users}
    res.send(resCreate);
  } catch (err) {
    next(err);
  }
});

//read one user
router.get("/:id", async (req, res, next) => {
  try {
    const user = await readUser(req.params.id);
    const resCreate: responseMessage = {status:200,message:`read user ${user.id}`,data:user}
    res.send(resCreate);
  } catch (err) {
    next(err);
  }
});

//read all users
router.get("/", async (req, res,next) => {
  try {
    let users = await readAllUsers();
    const resCreate: responseMessage = {status:200,message:`read all users`,data:users}
    res.json(resCreate);
  } catch (err) {
    next(err);
  }
});

export default router;
