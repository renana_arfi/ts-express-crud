import express, { Request, Response, NextFunction,ErrorRequestHandler,Errback } from "express";
import fs from "fs/promises";
import log from "@ajar/marker"
import {HttpException,responseErrorMessage} from "./types.js"
//middelware - log http request
export const updateLogFile = (path: string) => (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  fs.writeFile(path, `req id - ${req.id} = ${req.method}: ${req.path} >> ${Date.now()}\n`, {
    flag: "a",
  });
  next();
};

//middelware - printError

export const printError:ErrorRequestHandler= (err:HttpException,req,res,next)=>{
    log.red(err);
    next(err);
}

//middelware - update the LogError
export const updateLogError= (path:string) => (err:HttpException,req:Request,res:Response,next:NextFunction)=>{
    fs.writeFile(path,`${err.status} :: ${err.message} >> ${err.stack} \n`
    , {
        flag: "a",
      });
      next(err);
}
//middelware - response Error to client
export const responseError:ErrorRequestHandler= (err:HttpException,req,res,next)=>{
    const errRes: responseErrorMessage = {statusError:err.status,message:err.message,stack:err.stack}
    res.send(errRes);
}