import { getDB, updateDB } from "./db.js";
import {
  BadRequestException,
  UrlNotFoundException,
  HttpException,
  User,
} from "./types.js";
import { uniqeID } from "./string.js";

export async function readAllUsers(): Promise<User[]> {
  const users = await getDB();
  return users;
}

export async function readUser(idToRead: string): Promise<User> {
  const users = await getDB();
  const userToRead = users.find((user) => user.id === idToRead);
  if (userToRead === undefined) {
    throw new BadRequestException("id to read doesnt found");
  }
  return userToRead;
}

export async function deleteUser(idToDelete: string) {
  const users = await getDB();
  const indexDeleteUser = users.findIndex((user) => user.id === idToDelete);
  if (indexDeleteUser === -1) {
    throw new BadRequestException("id to delete doesnt found");
  }
  users.splice(indexDeleteUser, 1);
  updateDB(users);
  return users;
}

export async function createUser(
  firstName: string,
  lastName: string,
  email: string,
  password: number
) {
  const users = await getDB();
  for (const arg of arguments) {
    if (arg === undefined) {
      throw new BadRequestException(`You dont defined all your details`);
    }
  }
  const newUser: User = { id: uniqeID(), firstName, lastName, email, password };
  console.log(newUser);
  users.push(newUser);
  updateDB(users);
  return newUser;
}

export async function updateUser(idToUpdate: string, bodyParam: User) {
  const users = await getDB();
  const indexUserToUpdate = users.findIndex((user) => user.id === idToUpdate);
  if (indexUserToUpdate === -1) {
    throw new BadRequestException("id to update doesnt found");
  }
  const updateUser:User = { ...users[indexUserToUpdate], ...bodyParam };
  users[indexUserToUpdate] = updateUser;
  updateDB(users);
  return updateUser;
}
