import express, { RequestHandler } from "express";
import log from "@ajar/marker";
import usersRouter from "./usersRouter.js";
import {updateLogFile,printError,responseError,updateLogError} from "./logger.js"
import { UrlNotFoundException } from "./types.js";
import {uniqeID} from './string.js'
const { PORT = 3030, HOST = "localhost" } = process.env;
const LOG_FILE_PATH = "./req.http.log";
const LOG_ERROR_PATH = "./req.error.log";

const app = express();

app.use((req,res,next)=>{
  req.id = uniqeID();
  next();
})

app.use(express.json());
app.use(updateLogFile(LOG_FILE_PATH));
app.use("/user-manager" ,usersRouter);

app.use((req, res, next) => {
  const full_url = new URL(req.url, `http://${req.headers.host}`);
  next(new UrlNotFoundException(` - 404 - url ${full_url.href } was not found`))
});

app.use(printError);
app.use(updateLogError(LOG_ERROR_PATH));
app.use(responseError);

app.listen(Number(PORT), HOST, () => {
  log.magenta(`🌎  listening on`, `http://${HOST}:${PORT}`);
});
