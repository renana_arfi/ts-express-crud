export interface User {
  id: string;
  firstName: string;
  lastName: string;
  email: string;
  password: number;
}

export interface responseMessage{
  status: number;
  message: string;
  data: any;
}

export interface responseErrorMessage{
  statusError: number;
  message: string;
  stack: string | undefined;
}

export class HttpException extends Error {
  constructor(public message: string,public status: number) {
    super(message);
  }
}

export class UrlNotFoundException extends HttpException {
  constructor(public message: string,public status: number=404) {
    super(message,status);
  }
}

export class BadRequestException extends HttpException {
  constructor(public message: string,public status: number=400) {
    super(message,status);
  }
}

declare global {
  namespace Express {
    interface Request {
      id:string;
    }
  }
}
